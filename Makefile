DOCKER_COMPOSE := docker-compose
DOCKER_COMPOSE_EXEC := $(DOCKER_COMPOSE) exec
WORKSPACE := workspace
DOCKER_COMPOSE_EXEC_BACKEND =  $(DOCKER_COMPOSE_EXEC) $(WORKSPACE) 
COMPOSER := composer 

.PHONY: all

all: env init  migrate clear-cache

init: up timeout composer-install

env:
	cp .env.example .env; \
	cp src/.env.example src/.env 
	cp docker-compose.override.example.yml docker-compose.override.yml
	
ps:
	$(DOCKER_COMPOSE) ps

up:
	$(DOCKER_COMPOSE) up -d

stop:
	$(DOCKER_COMPOSE) stop 

down: 
	$(DOCKER_COMPOSE) down 

clean:
	$(DOCKER_COMPOSE) down -v

delete:
	$(DOCKER_COMPOSE) down -v --rmi all

ssh:
	$(DOCKER_COMPOSE_EXEC) $(WORKSPACE) bash 

migrate:
	$(DOCKER_COMPOSE_EXEC_BACKEND) php artisan migrate 

rollback:
	$(DOCKER_COMPOSE_EXEC_BACKEND) php artisan migrate:rollback

rollback-step:
	$(DOCKER_COMPOSE_EXEC_BACKEND) php artisan migrate:rollback --step=$(step)

clear-cache:
	$(DOCKER_COMPOSE_EXEC_BACKEND) php artisan clear-compiled; \
	$(DOCKER_COMPOSE_EXEC_BACKEND) php artisan view:clear; \
	$(DOCKER_COMPOSE_EXEC_BACKEND) php artisan config:clear; \
	$(DOCKER_COMPOSE_EXEC_BACKEND) php artisan cache:clear

swagger:
	$(DOCKER_COMPOSE_EXEC_BACKEND) php artisan l5-swagger:generate

composer-install:
	$(DOCKER_COMPOSE_EXEC_BACKEND) composer install

timeout:
	sleep 10

sync-sync:
	export PATH=$$PATH:~/.gem/ruby/2.6.0/bin; \
	docker-sync sync

sync-start:
	export PATH=$$PATH:~/.gem/ruby/2.6.0/bin; \
	docker-sync start

sync-stop:
	export PATH=$$PATH:~/.gem/ruby/2.6.0/bin; \
	docker-sync stop
	
	
sync-restart:
	export PATH=$$PATH:~/.gem/ruby/2.6.0/bin; \
	docker-sync stop; \
	docker-sync start; \

sync-clean:
	export PATH=$$PATH:~/.gem/ruby/2.6.0/bin; \
	docker-sync clean
